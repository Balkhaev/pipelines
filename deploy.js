const exec = require('child_process').execSync;

const commands = ['git status', 'git fetch --all', 'git reset --hard origin/master'];

const run = () =>
    commands.map(cmd => `====\ncmd: ${cmd}\n====\n\n${exec(cmd, { encoding: 'UTF8' })}`).join('\n--------------\n\n');

if (require.main === module) {
    run();
}

module.exports = run;
