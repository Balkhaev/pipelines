# Long lived branches

## Master
- Server = server.com (production)
- Commit = release
- Trust = true
```
pull from - release, hotfix
```

## Develop
- Server = qa.server.com
- Commit = non-tested feature
- Trust = mistrust
```
push - feature, hotfix
checkout from - master
```

# Short lived branches

## Release

```
merge in - master, develop
checkout from - staging
```

## Feature

```
