const Koa = require('koa');
const deploy = require('./deploy');

const app = new Koa();

app.use(async function(ctx) {
  ctx.body = deploy();
});

app.listen(3000);

console.log('Server started at 3000 port');
